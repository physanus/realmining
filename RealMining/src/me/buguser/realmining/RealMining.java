package me.buguser.realmining;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Random;

public class RealMining extends JavaPlugin implements Listener {

    
    /*
     * TODO
     * - set all the names to translatable strings
     */

	private FileConfiguration config = getConfig();

	@Override
	public void onEnable() {
		System.out.println("----------------------------------------------");
		System.out.println("          RealMining Pro activated            ");
		System.out.println("----------------------------------------------");

		registerEvents();

		config.addDefault("OverworldName", "world");
		config.addDefault("NetherName", "world_nether");
		config.addDefault("EndName", "world_end");
		config.addDefault("EventworldName", "world_event");

		config.options().copyDefaults(true);
		saveConfig();
	}

	@Override
	public void onDisable() {
		System.out.println("----------------------------------------------");
		System.out.println("        RealMining Pro deactivated            ");
		System.out.println("----------------------------------------------");
	}


	/**
	 * REgisters all the events used in this class
	 */
	private void registerEvents() {
		getServer().getPluginManager().registerEvents(this, this);
	}


	/**
	 * Main method managing the drops and side stuff
     */
	@EventHandler(priority = EventPriority.HIGH)
	public void onBreak(BlockBreakEvent event) {

		// Variable Initialisation
		Player player = event.getPlayer();
		Block block = event.getBlock();
		int random = (int) (Math.random() * 100); // number between 0 and 99
		int height = block.getY();

		// item related (dropped item)
		ItemStack droppedItem;
		ArrayList<String> droppedItemLore = new ArrayList<String>();
		String itemlore = ChatColor.GREEN + "➤ " + ChatColor.WHITE;	// the name of the item will be appended
		// off hand cannot be used to break blocks, so checking the main hand is enough
		int lootlevel = player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
		int silktouchlevel = player.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.SILK_TOUCH);


		// check Biome
		boolean biomeType1 =
				block.getBiome().name().equalsIgnoreCase("ICE_FLATS") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_ICE_FLATS") ||
				block.getBiome().name().equalsIgnoreCase("PLAINS") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_PLAINS") ||
				block.getBiome().name().equalsIgnoreCase("FOREST") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_FOREST") ||
				block.getBiome().name().equalsIgnoreCase("BIRCH_FOREST") ||
				block.getBiome().name().equalsIgnoreCase("BIRCH_FOREST_HILLS") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_BIRCH_FOREST") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_BIRCH_FOREST_HILLS") ||
				block.getBiome().name().equalsIgnoreCase("FOREST_HILLS") ||
				block.getBiome().name().equalsIgnoreCase("JUNGLE") ||
				block.getBiome().name().equalsIgnoreCase("JUNGLE_EDGE") ||
				block.getBiome().name().equalsIgnoreCase("JUNGLE_HILLS") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_JUNGLE") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_JUNGLE_EDGE ") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_ROOFED_FOREST") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_SWAMPLAND") ||
				block.getBiome().name().equalsIgnoreCase("ROOFED_FOREST") ||
				block.getBiome().name().equalsIgnoreCase("SWAMPLAND");

		boolean biomeType2 =
				block.getBiome().name().equalsIgnoreCase("DESERT") ||
				block.getBiome().name().equalsIgnoreCase("DESERT_HILLS") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_DESERT") ||
				block.getBiome().name().equalsIgnoreCase("SAVANNA") ||
				block.getBiome().name().equalsIgnoreCase("SAVANNA_ROCK") ||
				block.getBiome().name().equalsIgnoreCase("MESA") ||
				block.getBiome().name().equalsIgnoreCase("MESA_CLEAR_ROCK") ||
				block.getBiome().name().equalsIgnoreCase("MESA_ROCK") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_MESA") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_MESA_CLEAR_ROCK") ||
				block.getBiome().name().equalsIgnoreCase("MUTATED_MESA_ROCK");
		


		if (
            block.getMetadata("PLACED").isEmpty() &&
            (player.getGameMode().equals(GameMode.SURVIVAL) || player.getGameMode().equals(GameMode.ADVENTURE)) &&
            player.getInventory().getItemInMainHand().getItemMeta().hasLore() ) {
												
					
			// Check Pickaxes
			// TODO add pickaxe lore-names to config file
			boolean[] pickaxe = {
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Holzspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Steinspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Quarzspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Kupferspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Eisenspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Goldspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Silberspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Smaragdspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Diamantspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Rubinspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Amethystspitzhacke"),
					player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Netherspitzhacke")
			};
			
			// which quality did we use?
			boolean allPickaxes = false;
			boolean noWoodNoNether = false;
			boolean noWoodNoStone = false;	
			boolean noWoodNoStoneNoRuby = false;
			boolean noWoodNoStoneNoQuartz = false;
			boolean noWoodNoStoneNoNether = false;

            boolean quarzPickaxe = false;
            boolean emeraldPickaxe = false;
            boolean rubyPickaxe = false;
            boolean netherPickaxe = false;


			for(int i = 0; i<12; i++) {
				if(pickaxe[i]) {

					allPickaxes = true;
					if(i == 0) continue;

					if(i != 1) {
						noWoodNoStone = true;
						if(i != 2) noWoodNoStoneNoQuartz = true;
						if(i != 9) noWoodNoStoneNoRuby = true;
						if(i != 11) noWoodNoStoneNoNether = true;
					}

					if(i != 11) noWoodNoNether = true;

                    if(i == 3) quarzPickaxe = true;
                    if(i == 9) rubyPickaxe = true;
                    if(i == 7) emeraldPickaxe = true;
                    if(i == 11) netherPickaxe = true;

				}				
			}
						

			// nether auto melting
			if (block.getType() == Material.SAND && player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Netherschaufel")) {

				// drop glass instead of sand (melting the drop)
				event.setCancelled(true);
				block.setType(Material.AIR);
				block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.GLASS, 1));

			} else if (
					( block.getType() == Material.LOG ||
					  block.getType() == Material.LOG_2 )
					&&
					( player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Netheraxt") ||
					  player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Netherkampfaxt")) ) {

				// burn wood automatically to charcoal
				event.setCancelled(true);
				block.setType(Material.AIR);
				block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.COAL, 1, (short) 1));

			} else if (
					block.getType() == Material.STONE &&
					block.getData() == (byte) 0 &&
					netherPickaxe) {

				event.setCancelled(true);
				block.setType(Material.AIR);
				block.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STONE, 1));

			}




            /*
             *  OVERWORLD
             */

            if (player.getWorld().getName().equalsIgnoreCase(config.getString("OverworldName"))) {

                // Coal
                if (block.getType() == Material.COAL_ORE) {
                    if (allPickaxes) {

                        if (silktouchlevel > 0) {

                                itemlore += "Steinkohle";
                                droppedItemLore.add(itemlore);
                                drop(Material.GOLD_NUGGET, 1, droppedItemLore, 1, Material.AIR, event);

                                if(random < 20) // with a chance of 20%, place the block again
                                    block.setType(Material.COAL_ORE);

                        } else {
                            itemlore += "Kohle";
                            droppedItemLore.add(itemlore);

                            drop(Material.COAL, loot(lootlevel), droppedItemLore, 1, Material.AIR, event);
                        }


                    }
                }



                // Iron and Copper
                if (block.getType() == Material.IRON_ORE) {

                    if (height >= 48) {

                        // über 48: Kupfer
                        if (netherPickaxe) {
                            itemlore += "Kupferbarren";
                            droppedItemLore.add(itemlore);
                            drop(Material.IRON_INGOT, 1, droppedItemLore, 1, Material.AIR,event);
                        }
                        if (noWoodNoNether) {
                            itemlore += "Kupfererz";
                            droppedItemLore.add(itemlore);
                            drop(Material.GOLD_NUGGET, 1, droppedItemLore, 1, Material.AIR, event);
                        }

                    } else {

                        // unter 48: Eisen
                        if (netherPickaxe) {
                            itemlore += "Eisenbarren";
                            droppedItemLore.add(itemlore);
                            drop(Material.IRON_INGOT, 1, droppedItemLore, 2, Material.AIR, event);
                        }
                        if (noWoodNoNether) {
                            itemlore += "Eisenerz";
                            droppedItemLore.add(itemlore);
                            drop(Material.GOLD_NUGGET, 1, droppedItemLore, 2, Material.AIR, event);
                        }

                    }

                    if(silktouchlevel > 0 && random < 20) // with a chance of 20%, place the block again
                        block.setType(Material.IRON_ORE);

                }



                // Gold and Silver
                if (block.getType() == Material.GOLD_ORE) {
                    if(biomeType1) {

                        if (height >= 20){

                            // über 20: Silber
                            if (netherPickaxe) {
                                itemlore += "Silberbarren";
                                droppedItemLore.add(itemlore);
                                drop(Material.GOLD_INGOT, 1, droppedItemLore, 3, Material.AIR, event);
                            }
                            if (noWoodNoStoneNoNether) {
                                itemlore += "Silbererz";
                                droppedItemLore.add(itemlore);
                                drop(Material.GOLD_NUGGET, 1, droppedItemLore, 3, Material.AIR, event);
                            }

                        } else {

                            // unter 20: Gold
                            if (netherPickaxe) {
                                itemlore += "Goldbarren";
                                droppedItemLore.add(itemlore);
                                drop(Material.GOLD_INGOT, 1, droppedItemLore, 2, Material.AIR, event);
                            }
                            if (noWoodNoStoneNoNether) {
                                itemlore += "Golderz";
                                droppedItemLore.add(itemlore);
                                drop(Material.GOLD_NUGGET, 1, droppedItemLore, 2, Material.AIR, event);
                            }

                        }

                        if(silktouchlevel > 0 && random < 20) // with a chance of 20%, place the block again
                            block.setType(Material.GOLD_ORE);

                    }
                }



                // Lapis
                if (block.getType() == Material.LAPIS_ORE) {
                    if (noWoodNoStone) {

                        if (silktouchlevel > 0) {

                            // drop the ore
                            itemlore += "Lapislazulierz";
                            droppedItemLore.add(itemlore);
                            drop(Material.GOLD_NUGGET, 1, droppedItemLore, 3, Material.AIR, event);

                            if(random < 20) // with a chance of 20%, place the block again
                                block.setType(Material.LAPIS_ORE);

                        } else {

                            // drop the item
                            droppedItem = new ItemStack(Material.INK_SACK, 4 * loot(lootlevel), (short) 4);

                            ItemMeta im = droppedItem.getItemMeta();
                            im.setDisplayName(ChatColor.WHITE + "");
                            itemlore += "Lapislazuli";
                            droppedItemLore.add(itemlore);
                            im.setLore(droppedItemLore);
                            droppedItem.setItemMeta(im);

                            event.setCancelled(true);
                            block.setType(Material.AIR);
                            block.getWorld().dropItem(block.getLocation(), droppedItem);

                        }

                    }
                }



                // Redstone
                if (block.getType() == Material.REDSTONE_ORE || block.getType() == Material.GLOWING_REDSTONE_ORE) {
                    if (noWoodNoNether) {

                        if (silktouchlevel > 0) {

                            // drop the ore
                            itemlore += "Redstoneerz";
                            droppedItemLore.add(itemlore);
                            drop(Material.GOLD_NUGGET, 1, droppedItemLore, 3, Material.AIR, event);

                            if(random < 20) // with a chance of 20%, place the block again
                                block.setType(Material.GLOWING_REDSTONE_ORE);

                        } else {

                            // drop the item
                            itemlore += "Redstone";
                            droppedItemLore.add(itemlore);
                            drop(Material.REDSTONE, 4 * loot(lootlevel), droppedItemLore, 3, Material.AIR, event);

                        }

                    }
                }



                // Emerald and Ruby
                if (block.getType() == Material.EMERALD_ORE) {
                    if(biomeType2) {

                        if(height >= 49) {

                            // über 49: Emerald
                            if (emeraldPickaxe) {
                                itemlore += "Smaragd";
                                droppedItemLore.add(itemlore);
                                drop(Material.EMERALD, 2*loot(lootlevel), droppedItemLore, 6, Material.AIR, event);
                            }


                            if (noWoodNoStone) {
                                if (silktouchlevel > 0) {

                                    itemlore += "Smaragderz";
                                    droppedItemLore.add(itemlore);
                                    drop(Material.GOLD_NUGGET, 1, droppedItemLore, 6, Material.AIR, event);

                                    if(random < 20) // with a chance of 20%, place the block again
                                        block.setType(Material.EMERALD_ORE);

                                } else {
                                    itemlore += "Smaragd";
                                    droppedItemLore.add(itemlore);
                                    drop(Material.EMERALD, loot(lootlevel), droppedItemLore, 6, Material.AIR, event);
                                }
                            }

                        } else  {

                            // unter 48: Rubin
                            if (rubyPickaxe) {

                                itemlore += "Rubin";
                                droppedItemLore.add(itemlore);
                                drop(Material.EMERALD, 2*loot(lootlevel), droppedItemLore, 9, Material.AIR, event);

                            } else if(noWoodNoStone) {

                                if (silktouchlevel > 0) {
                                    itemlore += "Rubinerz";
                                    droppedItemLore.add(itemlore);
                                    drop(Material.GOLD_NUGGET, 1, droppedItemLore, 9, Material.AIR, event);

                                    if(random < 20) // with a chance of 20%, place the block again
                                        block.setType(Material.EMERALD_ORE);

                                } else {
                                    itemlore += "Rubin";
                                    droppedItemLore.add(itemlore);
                                    drop(Material.EMERALD, loot(lootlevel), droppedItemLore, 9, Material.AIR, event);
                                }


                            }

                        }

                    }
                }



                // Random Emerald-Tools
                if (block.getType().equals(Material.STONE)) {
                    if (emeraldPickaxe && random < 2) { // 2% change to drop an emerald from stone
                        itemlore += "Smaragd";
                        droppedItemLore.add(itemlore);
                        drop(Material.EMERALD, 1, droppedItemLore, 3, Material.AIR, event);
                    }
                }

                if (block.getType().equals(Material.SAND) || block.getType().equals(Material.GRASS) || block.getType().equals(Material.DIRT) || block.getType().equals(Material.GRAVEL)) {
                    if (player.getInventory().getItemInMainHand().getItemMeta().getLore().toString().contains("Smaragdschaufel") && random <= 1) { // 2% chance for a gold nugget
                        itemlore += "Goldklumpen";
                        droppedItemLore.add(itemlore);
                        drop(Material.GOLD_NUGGET, 1, droppedItemLore, 1, Material.AIR, event);
                    }
                }



                // Diamond and Amethyst
                if (block.getType() == Material.DIAMOND_ORE) {
                    if (noWoodNoStone) {

                        if (height <= 16 && biomeType2) {
                            // set diamonds to amethyst in biome2 under 16
                            if (silktouchlevel > 0) {
                                    itemlore += "Amethysterz";
                                    droppedItemLore.add(itemlore);
                                    drop(Material.GOLD_NUGGET, 1, droppedItemLore, 9, Material.AIR, event);

                                    if(random < 20)
                                        block.setType(Material.DIAMOND_ORE);

                            } else {
                                itemlore += "Amethyst";
                                droppedItemLore.add(itemlore);
                                drop(Material.DIAMOND, loot(lootlevel), droppedItemLore, 9, Material.AIR, event);
                            }

                        } else {

                            if (silktouchlevel > 0) {

                                    itemlore += "Diamanterz";
                                    droppedItemLore.add(itemlore);
                                    drop(Material.GOLD_NUGGET, 1, droppedItemLore, 7, Material.AIR, event);

                                    if(random < 20)
                                        block.setType(Material.DIAMOND_ORE);

                            } else {
                                itemlore += "Diamant";
                                droppedItemLore.add(itemlore);

                                drop(Material.DIAMOND, loot(lootlevel), droppedItemLore, 7, Material.AIR, event);
                            }
                        }
                    }
                }


            }



            /*
             *  NETHER
             */

            if (player.getWorld().getName().equalsIgnoreCase(config.getString("NetherName"))) {


                // Quarz
                if (block.getType() == Material.QUARTZ_ORE) {

                    if (noWoodNoStoneNoQuartz) {
                        itemlore += "Netherquarz";
                        droppedItemLore.add(itemlore);
                        drop(Material.QUARTZ, loot(lootlevel), droppedItemLore, 4, Material.AIR, event);
                    } else if (quarzPickaxe) {
                        itemlore += "Netherquarz";
                        droppedItemLore.add(itemlore);
                        drop(Material.QUARTZ, 2 * loot(lootlevel), droppedItemLore, 4, Material.AIR, event);
                    }

                    if(silktouchlevel > 0 && random < 20)
                        block.setType(Material.QUARTZ_ORE);

                }



                // Infernit
                if (block.getType() == Material.REDSTONE_ORE || block.getType() == Material.GLOWING_REDSTONE_ORE) {

                    if (noWoodNoStone) {
                        itemlore += "Infernit";
                        droppedItemLore.add(itemlore);
                        drop(Material.DIAMOND, 1, droppedItemLore, 15,Material.AIR, event);

                        if(silktouchlevel > 0 && random < 20)
                            block.setType(Material.GLOWING_REDSTONE_ORE);
                    }

                }



                // Amethyst
                if (block.getType() == Material.DIAMOND_ORE) {

                    if (noWoodNoStone) {
                        itemlore += "Amethyst";
                        droppedItemLore.add(itemlore);
                        drop(Material.DIAMOND, 1, droppedItemLore, 9, Material.AIR, event);

                        if(silktouchlevel > 0 && random < 20)
                            block.setType(Material.DIAMOND_ORE);
                    }

                }



                // Soulshard
                if (block.getType() == Material.LAPIS_ORE) {

                    if (noWoodNoStone) {
                        itemlore += "Seelenscherbe";
                        droppedItemLore.add(itemlore);
                        drop(Material.IRON_INGOT, 1, droppedItemLore, 3, Material.AIR, event);

                        if(silktouchlevel > 0 && random < 20)
                            block.setType(Material.LAPIS_ORE);
                    }

                }



                // Wither Essence
                if ((block.getType() == Material.COAL_ORE)) {

                    if (allPickaxes) {
                        itemlore += "Witheressenz";
                        droppedItemLore.add(itemlore);
                        drop(Material.IRON_INGOT, 1, droppedItemLore, 3, Material.AIR, event);

                        if(silktouchlevel > 0 && random < 20)
                            block.setType(Material.COAL_ORE);
                    }

                }



                // Fiery Gold
                if ((block.getType() == Material.GOLD_ORE)) {

                    if (noWoodNoStone) {
                        itemlore += "Feuriger Goldbarren";
                        droppedItemLore.add(itemlore);
                        drop(Material.GOLD_INGOT, 1, droppedItemLore, 5, Material.AIR,event);

                        if(silktouchlevel > 0 && random < 20)
                            block.setType(Material.GOLD_ORE);
                    }

                }
						
						
            }



            /*
             *  NETHER
             */

            if (player.getWorld().getName().equalsIgnoreCase(config.getString("EndName"))) {


                // Amethyst
                if (block.getType() == Material.DIAMOND_ORE) {

                    if (noWoodNoStone) {
                        itemlore += "Amethyst";
                        droppedItemLore.add(itemlore);
                        drop(Material.DIAMOND, 1, droppedItemLore, 9, Material.AIR, event);

                        if(silktouchlevel > 0 && random < 20)
                            block.setType(Material.DIAMOND_ORE);
                    }

                }


            }


        } //End of essential check: lore, gamemode, metadata
				
		
		

		
		if (player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("pickaxe") && (!player.getInventory().getItemInMainHand().getItemMeta().hasLore())) {
			if (player.getGameMode() == GameMode.SURVIVAL || player.getGameMode() == GameMode.ADVENTURE) {

                // Tell the player that a pickace w/o a valie lore may not be used
                // (they could get an ore and place it in another biome to get a different loot from it)
                event.setCancelled(true);
				player.sendMessage(ChatColor.RED + "Du darfst diese Spitzhacke nicht verwenden!\n" +
                        ChatColor.GRAY + "Bei Fragen wende dich an ein Teammitglied");

			}
		}
	}


    /**
     * calculates loot, especially when enchanted with luck
     * @param enchlevel Luck 1, 2 or 3
     * @return drop count
     */
	private static int loot(int enchlevel) {

        if(enchlevel == 0) return 1;

		int dropcount = 1; // how many drops there will be
		double target = 1.0 / (enchlevel + 2); //
        //   1    2     3    luck = enchlevel
        //  1/3  1/4   1/5  target

        // dropcount will be greater with enchlevel being greater
        Random random = new Random();
		for (int i = 0; i < enchlevel; i++) {
            double rand = random.nextDouble();
			if (rand < target)
                dropcount++;
		}

		return dropcount;
	}


    /**
     * replaces block, defines new drop & exp etc.
     */
	public static void drop(Material mat, int count, ArrayList<String> lore, int exp,Material replace, BlockBreakEvent event) {
		
		ItemStack it = new ItemStack(mat, count);
		ItemMeta im = it.getItemMeta();
		im.setDisplayName(ChatColor.WHITE + "");
		im.setLore(lore);
		it.setItemMeta(im);

		event.setCancelled(true);
		event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), it);
		event.getBlock().setType(replace);

		if (exp != 0)
			event.getPlayer().getWorld().spawn(event.getBlock().getLocation(), ExperienceOrb.class).setExperience(exp);
	}

	@EventHandler
	public void blockPlaced(BlockPlaceEvent event) {

        // mark the user-placed blocks as userplaced
        // usually users should not have access to raw ores since the drop is modified, even with silktouch
		if (!(event.getPlayer().getGameMode() == GameMode.CREATIVE)) {
			Block b = event.getBlock();
			b.setMetadata("PLACED", new FixedMetadataValue(this, "true")); // Set marker
		}

	}

}
